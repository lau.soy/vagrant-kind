# Créer un bac à sable Kubernetes avec Vagrant et Kind

## Prérequis

Installer [Virtualbox](https://www.virtualbox.org/wiki/Downloads) et [vagrant](https://www.vagrantup.com/docs/installation).

## Créer la VM

Se positionner au même niveau que le fichier `Vagrantfile` et passer la commande `vagrant up`.

## Détails

Seront installés:
- docker CE
- Go
- kubectl
- kind
- un cluster K8S nommé "dev" dans kind

l'API sera exposée sur https://10.0.0.11:6443

Le fichier de config est dans `/home/vragrant/.kube/config`, on peut le copier sur l'host pour utiliser une CLI locale ou une IDE K8S (ex: [Lens](https://k8slens.dev/).
Commande scp `scp -i .\.vagrant\machines\docker1\virtualbox\private_key vagrant@10.0.0.11:/home/vagrant/.kube/config .` (depuis la racine du projet).
