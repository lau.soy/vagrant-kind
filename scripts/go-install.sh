#!/bin/bash
cd ~

curl https://dl.google.com/go/go1.16.4.linux-amd64.tar.gz --output go1.16.4.linux-amd64.tar.gz -#
sudo tar -xvf go1.16.4.linux-amd64.tar.gz -C /usr/local
rm go1.16.4.linux-amd64.tar.gz

mkdir $HOME/go

echo "export GOROOT=/usr/local/go" > .go_env
echo "export GOPATH=$HOME/go" >> .go_env
echo "export PATH=$PATH:/usr/local/go/bin:$HOME/go/bin" >> .go_env

echo "source .go_env" >> .profile
source .profile

go version



